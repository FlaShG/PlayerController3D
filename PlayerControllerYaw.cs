﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    [DisallowMultipleComponent]
    public class PlayerControllerYaw : MonoBehaviour
    {
        new private Transform transform;
        [Range(1f, 10f)]
        [SerializeField]
        private float speed = 5f;
        internal System.Func<float> pollInput;


        private void Awake()
        {
            transform = base.transform;
        }

        private void Update()
        {
            if (pollInput != null)
            {
                transform.Rotate(Vector3.up * pollInput() * speed);
            }
        }
    }
}