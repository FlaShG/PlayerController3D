﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerControllerComponentJump : PlayerControllerComponentBase
    {
        public override string title => "Ground Jump";

        [Range(0.1f, 50f)]
        public float power = 10f;

        public override void UpdateState(ref PlayerControllerMovement3DBase.State state)
        {
            if (state.isGrounded && state.input.jump)
            {
                state.velocity.y = power;
            }
        }
    }
}
