﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerControllerComponentGravity : PlayerControllerComponentBase
    {
        public override string title => "Gravity";

        [Range(0.1f, 50f)]
        public float acceleration = 10f;

        public override void UpdateState(ref PlayerControllerMovement3DBase.State state)
        {
            if (!state.isGrounded)
            {
                state.velocity.y -= acceleration * Time.deltaTime;
            }
        }
    }
}
