﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerControllerComponentRun : PlayerControllerComponentBase
    {
        public override string title => "Ground Acceleration";

        [Range(1f, 50f)]
        public float maxSpeed = 10f;
        [Range(0.1f, 50f)]
        public float acceleration = 40f;

        public override void UpdateState(ref PlayerControllerMovement3DBase.State state)
        {
            if (state.isGrounded)
            {
                var input = state.input.horizontal;
                var targetVelocity = new Vector3(input.x, 0, input.y) * maxSpeed;

                state.velocity = Vector3.MoveTowards(state.velocity, targetVelocity, acceleration * Time.deltaTime);
                state.velocity.y = -1;
            }
        }
    }
}
