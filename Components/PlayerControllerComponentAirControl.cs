﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerControllerComponentAirControl : PlayerControllerComponentBase
    {
        public override string title => "Air Acceleration";

        [Range(1f, 50f)]
        public float maxSpeed = 10f;
        [Range(0.1f, 50f)]
        public float acceleration = 20f;

        public override void UpdateState(ref PlayerControllerMovement3DBase.State state)
        {
            if (!state.isGrounded && state.input.horizontal != Vector2.zero)
            {
                var verticalVelocity = state.velocity.y;

                var input = state.input.horizontal;
                var targetVelocity = new Vector3(input.x, 0, input.y) * maxSpeed;

                state.velocity = Vector3.MoveTowards(state.velocity, targetVelocity, acceleration * Time.deltaTime);
                state.velocity.y = verticalVelocity;
            }
        }
    }
}
