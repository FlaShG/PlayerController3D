﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerControllerComponentMultiJump : PlayerControllerComponentBase
    {
        public override string title => "Multi Jump";

        [Range(0.1f, 50f)]
        public float power = 10f;
        [Tooltip("Amount of jumps that can be performed after touching the ground. 0 = infinite.")]
        [Range(0, 10)]
        public int airJumpCount = 1;
        private bool hasInfiniteJumps => airJumpCount == 0;
        private int currentJumpCount;

        public override void UpdateState(ref PlayerControllerMovement3DBase.State state)
        {
            if (state.isGrounded)
            {
                currentJumpCount = airJumpCount > 0 ? airJumpCount : -1;
            }
            else
            {
                if (state.input.jump && (hasInfiniteJumps || currentJumpCount > 0))
                {
                    state.velocity.y = power;

                    if (!hasInfiniteJumps)
                    {
                        currentJumpCount--;
                    }
                }
            }
        }
    }
}
