﻿
namespace ThirteenPixels.PlayerController
{
    using ThirteenPixels.MiniComponents;

    public abstract class PlayerControllerComponentBase : MiniComponentBase
    {
        public abstract void UpdateState(ref PlayerControllerMovement3DBase.State state);
    }
}
