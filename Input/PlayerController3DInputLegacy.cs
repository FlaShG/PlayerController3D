﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    public class PlayerController3DInputLegacy : PlayerController3DInputBase
    {
        private bool jumpInputBuffer;
        [SerializeField]
        private string horizontalAxis = "Horizontal";
        [SerializeField]
        private string verticalAxis = "Vertical";
        [SerializeField]
        private string jumpButton = "Jump";
        [SerializeField]
        private string cameraYawAxis = "Mouse X";
        [SerializeField]
        private bool cameraYawAxisIsMouse = true;

        protected virtual void Update()
        {
            if (Input.GetButtonDown(jumpButton))
            {
                jumpInputBuffer = true;
            }
        }

        protected override PlayerControllerMovement3DBase.State.Input PollInput()
        {
            var horizontal = new Vector2(Input.GetAxisRaw(horizontalAxis), Input.GetAxisRaw(verticalAxis));
            if (horizontal != Vector2.zero)
            {
                horizontal = horizontal.normalized;
            }

            var result = new PlayerControllerMovement3DBase.State.Input
            {
                horizontal = horizontal,
                jump = jumpInputBuffer
            };

            jumpInputBuffer = false;

            return result;
        }

        protected override float PollYawInput()
        {
            return Input.GetAxis(cameraYawAxis) * (cameraYawAxisIsMouse ? 1 : Time.deltaTime);
        }
    }
}
