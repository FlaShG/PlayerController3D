﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    [DisallowMultipleComponent]
    [RequireComponent(typeof(PlayerControllerMovement3DBase))]
    public abstract class PlayerController3DInputBase : MonoBehaviour
    {
        private PlayerControllerMovement3DBase controller;
        private PlayerControllerYaw yaw;

        protected virtual void Awake()
        {
            controller = GetComponent<PlayerControllerMovement3DBase>();
            yaw = GetComponent<PlayerControllerYaw>();
        }

        protected virtual void OnEnable()
        {
            controller.pollInput = PollInput;
            if (yaw)
            {
                yaw.pollInput = PollYawInput;
            }
        }

        protected virtual void OnDisable()
        {
            controller.pollInput = null;
            if (yaw)
            {
                yaw.pollInput = null;
            }
        }

        protected abstract PlayerControllerMovement3DBase.State.Input PollInput();
        protected abstract float PollYawInput();
    }
}
