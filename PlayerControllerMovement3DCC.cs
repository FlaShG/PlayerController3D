﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;

    [RequireComponent(typeof(CharacterController))]
    public class PlayerControllerMovement3DCC : PlayerControllerMovement3DBase
    {
        private CharacterController characterController;
        private Vector3 lastMoveResult;


        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
        }

        private void FixedUpdate()
        {
            UpdateState();

            var previousPosition = transform.position;
            characterController.Move(transform.TransformDirection(state.velocity) * Time.deltaTime);

            lastMoveResult = (transform.position - previousPosition) / Time.deltaTime;
        }

        protected override bool CalculateIsGrounded()
        {
            return characterController.isGrounded;
        }

        protected override Vector3 GetLastMoveResult()
        {
            return lastMoveResult;
        }
    }
}
