﻿
namespace ThirteenPixels.PlayerController
{
    using UnityEngine;
    using ThirteenPixels.MiniComponents;
    
    /// <summary>
    /// 3D Player Controller base class. Extend to use its mechanics with a CharacterController, Rigidbody or custom physics.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class PlayerControllerMovement3DBase : MonoBehaviour
    {
        [System.Serializable]
        protected class ComponentContainer : MiniComponentContainerBase<PlayerControllerComponentBase> { }

        public struct State
        {
            public struct Input
            {
                public Vector2 horizontal;
                public bool jump;
            }

            public Vector3 velocity;
            public bool isGrounded;
            public Input input;
        }

        [Tooltip("How much the last velocity should be lerped towards the last actual movement vector. A higher value means a higher velocity change upon collisions.")]
        [Range(0f, 1f)]
        [SerializeField]
        protected float collisionFactor = 1f;
        [SerializeField]
        protected ComponentContainer components = default;

        protected State state;
        internal System.Func<State.Input> pollInput;


        protected void UpdateState()
        {
            var lastMoveResult = transform.InverseTransformDirection(GetLastMoveResult());
            state.velocity = Vector3.Lerp(state.velocity, lastMoveResult, collisionFactor);

            state.input = pollInput != null ? pollInput() : default;
            state.isGrounded = CalculateIsGrounded();

            foreach (var component in components)
            {
                component.UpdateState(ref state);
            }
        }

        protected abstract bool CalculateIsGrounded();
        protected abstract Vector3 GetLastMoveResult();
    }
}
